import openai
import textwrap
import os
import re

openai.api_key = os.getenv("OPENAI_API_KEY")


def docstring(file, temperature=0.1, max_tokens=300, file_type='python'):

    # read the file we want to analyze
    _file = open(file).read()

    # Define a regular expression pattern to match URLs
    url_pattern = r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'

    # Replace all URLs in the file with an anonymized string
    anonymized_contents = re.sub(url_pattern, 'ANONYMIZED_URL', _file)
    # add a prompt of what we want to do
    prompt = anonymized_contents + f"can you give me a short description for the above {file_type} script:\n\"\"\""

    # call the model
    response = openai.Completion.create(
        model="text-davinci-003",
        prompt=prompt,
        temperature=temperature,
        max_tokens=max_tokens,
        top_p=1.0,
        frequency_penalty=0.0,
        presence_penalty=0.0,
        stop=["#", "\"\"\""]
    )

    # Print the putput
    description = response["choices"][0]["text"]
    wrapped_string = textwrap.fill(description, width=70, initial_indent='    ', subsequent_indent='    ')
    print(wrapped_string)
